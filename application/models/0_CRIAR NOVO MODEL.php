<?php

class FinanceiroContas_model extends CI_Model {
    
    //Nome da tabela
    protected $_table = "fin_contas";
    //Chave primaria
    protected $_primary_key = "idContas";

     //Metodo para inserir
    public function insert($dados) {
        return $this->db->insert($this->_table, $this->db->escape($dados));
    }
    //Metodo para update
    public function update($dados, $id) {
        return $this->db->update($this->_table, $dados, "$this->_primary_key = $id");
    }
    //Metodo para exclusao
    public function delete($dados) {
        return $this->db->delete($this->_table, $dados);
    }
    
    //Listagem - grid
    public function listar() {
        
        $sql = "Select
                    b.idBanco,
                    b.descBanco,
                    c.idContas,
                    c.nomeReferencia,
                    c.numAgencia,
                    c.numConta,
                    c.sitStatusConta,
                    c.sitExclusao
                  From
                     fin_contas c Inner Join
                     fin_bancos b On c.idBanco = b.idBanco
                  Where
                    c.sitExclusao = 'N'                
                ";
        
       
        //executando query
        $query = $this->db->query($sql);
        //retornando consulta
        return $query->result();

    }
    
    //Resgatando registro apartir de um id
    public function getId($id){
        
           $sql = "Select
                    b.idBanco,
                    b.descBanco,
                    c.nomeReferencia,
                    c.numAgencia,
                    c.numConta,
                    c.sitStatusConta,
                    c.sitExclusao,
                    c.idContas,
                    t.idTipoConta,
                    t.descTipoConta,
                    c.nomeReferencia,
                    c.saldoInicial,
                    c.dthSaldoInicial
                  From
                     fin_contas c Inner Join
                     fin_bancos b On c.idBanco = b.idBanco Inner Join
                     fin_tipoconta t On c.idTipoConta = t.idTipoConta
                  Where
                    c.sitExclusao = 'N' And
                    c.idContas = $id              
                ";

           //executando query
           $query = $this->db->query($sql);
           //retornando consulta
           return $query->row();
        
    }

    //Verifica se ja existe registro
    public function se_exitir($where) {
        //$where => array vindo do controller
        $this->db->where($where);      
        //resultados da consulta
        $query = $this->db->get($this->_table);    
        //Verifica a quantidade de ocorrencias
        if($query->num_rows > 0) {
            //se for maior que 0 => existe registro no banco
            return false;
        }else {
            //nao existe
            return true;
        }
    }
    
    public function listarBancos() {
        //retornando resultados da consulta
        return $this->db->get('fin_bancos')->result();       
        
    }
    
    public function listarTiposConta() {
        //retornando resultados da consulta
        return $this->db->get('fin_tipoconta')->result();       
        
    }

   

}
