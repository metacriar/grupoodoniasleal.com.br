        <section id="prices"><div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="block-header">Our Pricing Plans</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, cum, odio ipsa tenetur neque quod eius aliquam assumenda. Quae blanditiis molestiae reiciendis consectetur ratione quam natus accusantium maiores aliquam iste.</p>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-3">
                        <ul class="pricing-table">
                            <li class="plan-price"><span>$</span> <span>99</span> <p>Per Month</p></li>
                            <li class="plan-name"><p>Basic Plan</p></li>
                            <li class="features-list">
                                <ul>
                                    <li>Feature 01</li>
                                    <li>Feature 02</li>
                                    <li class="feature_disabled">Feature 03</li>
                                    <li class="feature_disabled">Feature 04</li>
                                    <li class="feature_disabled">Feature 05</li>
                                </ul>
                            </li>
                            <li class="call-to-action"><a href="#" class="theme_btn">Sign Up Now</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3">
                        <ul class="pricing-table">
                            <li class="plan-price"><span>$</span> <span>299</span> <p>Per Month</p></li>
                            <li class="plan-name"><p>Advanced Plan</p></li>
                            <li class="features-list">
                                <ul>
                                    <li>Feature 01</li>
                                    <li>Feature 02</li>
                                    <li>Feature 03</li>
                                    <li class="feature_disabled">Feature 04</li>
                                    <li class="feature_disabled">Feature 05</li>
                                </ul>
                            </li>
                            <li class="call-to-action"><a href="#" class="theme_btn">Sign Up Now</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3">
                        <ul class="pricing-table">
                            <li class="plan-price"><span>$</span> <span>399</span> <p>Per Month</p></li>
                            <li class="plan-name"><p>Premium Plan</p></li>
                            <li class="features-list">
                                <ul>
                                    <li>Feature 01</li>
                                    <li>Feature 02</li>
                                    <li>Feature 03</li>
                                    <li>Feature 04</li>
                                    <li class="feature_disabled">Feature 05</li>
                                </ul>
                            </li>
                            <li class="call-to-action"><a href="#" class="theme_btn">Sign Up Now</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-3">
                        <ul class="pricing-table">
                            <li class="plan-price"><span>$</span> <span>499</span> <p>Per Month</p></li>
                            <li class="plan-name"><p>Professional Plan</p></li>
                            <li class="features-list">
                                <ul>
                                    <li>Feature 01</li>
                                    <li>Feature 02</li>
                                    <li>Feature 03</li>
                                    <li>Feature 04</li>
                                    <li>Feature 05</li>
                                </ul>
                            </li>
                            <li class="call-to-action"><a href="#" class="theme_btn">Sign Up Now</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </section>