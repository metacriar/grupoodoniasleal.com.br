<footer id="footer" class="grey_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2 class="block-header">Entre em contato</h2>
                <p>Se você tem alguma dúvida ou deseja mas detalhes sobre nosso escritorio entre em contato com nossa equipe.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="contact-form">
                    <form class="contact-form" method="post" action="/">
                        <p class="contact-form-name">
                            <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nome Completo">
                        </p>
                        <p class="contact-form-email">
                            <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email">
                        </p>
                        <p class="contact-form-subject">
                            <input type="text" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Assunto">
                        </p>
                        <p class="contact-form-message">
                            <textarea aria-required="true" rows="8" cols="45" name="message" id="message" class="form-control" placeholder=""></textarea>
                        </p>
                        <p class="contact-form-submit text-center">
                            <input type="submit" value="Enviar Mensagem" id="contact_form_submit" name="contact_submit" class="theme_btn">
                        </p>
                    </form>
                </div>
            </div>
            <div class="block widget_text col-sm-6">
                <h3>Grupo Odonias Leal</h3>
                <p>
                    Há mais de 30 anos em defesa dos que tem fome e sede de justiça<br> 
                    <span><strong>Telefone:</strong> </span>(086) 3083-0534<br>
                    <span><strong>Email:</strong> </span>grupoodoniasleal@hotmail.com<br> 
                    <span><strong>Endereço:</strong> </span>Avenida Miguel Rosa, 6120 / Sul
                    <br>
                    Localizado em uma das principais avenidas de Teresina, com fácil acesso
                    e amplo estacionamento, o escritório dispõe de confortáveis e modernas instalações
                    lhe oferecendo, praticidade e comodidade na hora de contratar o Grupo Odonias leal
                    para defender o seu direito.
                </p>
                <p>
                    Não perca mais tempo comprometendo a realização do seu direito, nos procure ou agende o
                    seu atendimento personalizado hoje mesmo!
                </p>

            </div>

        </div>
    </div>
</footer>