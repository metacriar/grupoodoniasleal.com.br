<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('head') ?>
    <body>
        <div id="top"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <?php $this->load->view('home_view'); ?>
        <?php $this->load->view('menu_view'); ?>
        <?php $this->load->view('servicos_view'); ?>
        <?php $this->load->view('indicadores_view'); ?>
        <?php $this->load->view('equipe_view'); ?>
        <?php $this->load->view('clientes_view'); ?> 
        <?php $this->load->view('depoimentos_view'); ?>
        <!--  $this->load->view('precos_view'); ?> -->
        <?php $this->load->view('footer_view'); ?>

        <section id="map_wrap">
            <div id="map"></div> 
        </section>

        <section id="copyright" class="dark_section">
            <div class="container"><div class="row">

                    <div class="col-sm-12"><p class="text-center">&copy; 2015 Grupo Odonias Leal. Todos os Direitos Reservados</p></div>

                </div></div>
        </section>

        <div class="preloader">
            <div class="preloaderimg"></div>
        </div>

        <div id="gallery_container"></div>

        <script src="<?php base_url() ?>assets/js/vendor/respond.min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/placeholders.min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery-1.10.2.min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/bootstrap.min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/hoverIntent.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/superfish.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.actual.min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.elastislide.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.flexslider-min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.prettyPhoto.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.easing.1.3.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.ui.totop.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.isotope.min.js"></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.easypiechart.min.js"></script>
        <script src='<?php base_url() ?>assets/js/vendor/jflickrfeed.min.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.timelinr-0.9.54.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.fitvids.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.bxslider.min.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.parallax-1.1.3.js'></script>
        <script src="<?php base_url() ?>assets/js/vendor/jquery.sticky.js"></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.scrollTo-min.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.localscroll-min.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/owl.carousel.min.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.nicescroll.min.js'></script>
        <script src='<?php base_url() ?>assets/js/vendor/jquery.funnyText.min.js'></script>
        <script src='<?php base_url() ?>assets/twitter/jquery.tweet.min.js'></script>
        <script src="<?php base_url() ?>assets/js/plugins.js"></script>
        <script src="<?php base_url() ?>assets/js/main.js"></script>

        <!-- Map Scripts -->

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript">
            var lat;
            var lng;
            var map;

            //type your address after "address="
            jQuery.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=28 Avenida Miguel Rosa, 6120 / Sul US&sensor=false', function(data) {
                lat = data.results[0].geometry.location.lat;
                lng = data.results[0].geometry.location.lng;
            }).complete(function() {
                dxmapLoadMap();
            });

            function attachSecretMessage(marker, message)
            {
                var infowindow = new google.maps.InfoWindow(
                        {content: message
                        });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            }

            window.dxmapLoadMap = function()
            {
                var center = new google.maps.LatLng(lat, lng);
                var settings = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 16,
                    draggable: false,
                    scrollwheel: false,
                    center: center
                };
                map = new google.maps.Map(document.getElementById('map'), settings);

                var marker = new google.maps.Marker({
                    position: center,
                    title: 'Map title',
                    map: map
                });
                marker.setTitle('Map title'.toString());
                //type your map title and description here
                attachSecretMessage(marker, '<h3>Map title</h3>Map HTML description');
            }
        </script>
    </body>
</html>