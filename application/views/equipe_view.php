<section id="team" class="grey_section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="block-header">Nossa equipe</h2>
                    </div>
                </div>

                <div class="row">

                    <div class="block col-sm-3">
                        <div class="thumbnail">
                            <img src="<?php base_url()?>assets/example/team_man5.jpg" alt="team">
                            <div class="caption">
                                <h4>Odonias Leal da Luz</h4>
                                <p>
                                    Piauiense de Inhuma, Odonias Leal da Luz, 54 anos, Diretor-presidente do grupo juridico que leva o seu nome.
                                </p>
                                <p>
                                    <a class="socialico-twitter" href="#" title="Twitter">#</a>
                                    <a class="socialico-facebook" href="#" title="Facebook">#</a>
                                    <a class="socialico-linkedin" href="#" title="Google">#</a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="block col-sm-3">
                        <div class="thumbnail">
                            <img src="<?php base_url()?>assets/example/team_woman4.jpg" alt="team">
                            <div class="caption">
                                <h4>Solange Leal</h4>
                                <p>
                                   Responsável administrativo e financeiro do Grupo Odonias Leal 
                                </p>
                                <p>
                                    <a class="socialico-twitter" href="#" title="Twitter">#</a>
                                    <a class="socialico-facebook" href="#" title="Facebook">#</a>
                                    <a class="socialico-linkedin" href="#" title="Google">#</a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="block col-sm-3">
                        <div class="thumbnail">
                            <img src="<?php base_url()?>assets/example/team_man3.jpg" alt="team">
                            <div class="caption">
                                <h4>Raimundo Reginaldo</h4>
                                <p>
                                Experiente advogado civilista e um dos sócios do escritório.
                                </p>
                                <p>
                                    <a class="socialico-twitter" href="#" title="Twitter">#</a>
                                    <a class="socialico-facebook" href="#" title="Facebook">#</a>
                                    <a class="socialico-linkedin" href="#" title="Google">#</a>
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="block col-sm-3">
                        <div class="thumbnail">
                            <img src="<?php base_url()?>assets/example/team_woman1.jpg" alt="team">
                            <div class="caption">
                                <h4>Ronaldo A. Gualberto</h4>
                                <p>
                                Advogado atuante e conceituado em ações previdenciárias de âmbito administrativo
                                ou judicial.
                                </p>
                                <p>
                                    <a class="socialico-twitter" href="#" title="Twitter">#</a>
                                    <a class="socialico-facebook" href="#" title="Facebook">#</a>
                                    <a class="socialico-linkedin" href="#" title="Google">#</a>
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </section>
