<section id="land" class="parallax dark_section text-center">
            <div class="container">


                <div class="row">
                    <div class="col-sm-12">
                        <a class="navbar-brand" href="./">
                            <img src="<?php base_url()?>assets/example/logo-big.png" alt="">A serviço dos que tem fome e sede de Justiça</a>
                    </div>
                    <div class="block col-sm-3">
                        <a href="#bank_disputes">
                            <div class="single_teaser icons style5">
                                <div class="image-icon">
                                    <i class="rt-icon-banknote"></i>
                                </div>
                                <h3>Bancos</h3>
                                <span class="rt-icon-link2"></span>
                            </div>
                        </a>
                    </div>

                    <div class="block col-sm-3">
                        <a href="#family_disputes">        
                            <div class="single_teaser icons style5">
                                <div class="image-icon">
                                    <i class="rt-icon-users3"></i>
                                </div>
                                <h3>Família</h3>
                                <span class="rt-icon-link2"></span>
                            </div>
                        </a>
                    </div>

                    <div class="block col-sm-3">
                        <a href="#business_disputes">
                            <div class="single_teaser icons style5">
                                <div class="image-icon">
                                    <i class="rt-icon-world"></i>
                                </div>
                                <h3>INSS</h3>
                                <span class="rt-icon-link2"></span>
                            </div>
                        </a>
                    </div>

                    <div class="block col-sm-3">
                        <a href="#copyright_disputes">
                            <div class="single_teaser icons style5">
                                <div class="image-icon">
                                    <i class="rt-icon-bookmark"></i>
                                </div>
                                <h3>Empresas</h3>
                                <span class="rt-icon-link2"></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>