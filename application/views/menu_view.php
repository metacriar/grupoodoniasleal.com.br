<section id="header">
    <div class="container"><div class="row">

            <a class="navbar-brand" href="./index.html"><img src="<?php base_url() ?>assets/example/logo.png" alt=""><span>Law Alliance</span></a>

            <div class="col-sm-12 mainmenu_wrap"><div class="main-menu-icon visible-xs"><span></span><span></span><span></span></div>
                <ul id="mainmenu" class="nav menu sf-menu responsive-menu superfish">
                    <li class="">
                        <a href="#top">Inicio</a>
                    </li>
                    <li class="">
                        <a href="#features">Serviços</a>
                    </li>
                    <li class="">
                        <a href="#progress">Sobre</a>
                    </li>
                    <li class="">
                        <a href="#team">Equipe</a>
                    </li>
                    <li class="">
                        <a href="#belowcontent">Clientes</a>
                    </li>
                    <li class="">
                        <a href="#testimonials">Notícias</a>
                    </li>
 <!--                    <li class="">
                        <a href="#prices">Preços</a>
                    </li>          
-->                 <li class="">
                        <a href="#footer">Contato</a>
                    </li>
                </ul>
            </div>

        </div></div>
</section>