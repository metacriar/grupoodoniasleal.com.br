<head>
    <meta charset="utf-8">

    <title>Grupo Odonias Leal - Sociedade de Advogados</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php base_url() ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php base_url() ?>assets/css/main.css">
    <script src="<?php base_url() ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
</head>