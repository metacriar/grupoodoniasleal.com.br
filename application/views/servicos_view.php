<section id="features" class="grey_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2 class="block-header">Serviços</h2>
            </div>
        </div>

        <div class="row">

            <div class="block col-sm-6">
                <div class="left_icons" id="bank_disputes">
                    <div class="dxsingle_teaser_left">
                        <i class="rt-icon-banknote"></i>
                    </div>
                    <div class="dxsingle_teaser_right">
                        <h3>Bancos</h3>
                        <p>
                            Temos uma equipe habilitada e experiente para as diversas causas 
                            envolvendo Bancos ou outras instituições financeiras (Revisões de Contrato,
                            Busca e Apreensão, etc.)
                        <p><a href="#footer">Solicite contato para maiores informações</a></p>
                        </p>
                    </div>
                </div>
            </div>


            <div class="block col-sm-6">
                <div class="left_icons style2" id="business_disputes">
                    <div class="dxsingle_teaser_left">
                        <i class="rt-icon-world"></i>
                    </div>
                    <div class="dxsingle_teaser_right">
                        <h3>INSS</h3>
                        <p>
                            Ações que envolvam o INSS e demais órgãos previdenciários, demandam estratégia,
                            conhecimento profundo da legislação e constante atualização dos atos administrativos
                            que influenciam na concessão dos benefícios.
                        <p><a href="#footer">Solicite contato para maiores informações</a></p>
                        </p>
                    </div>
                </div>
            </div>


            <div class="block col-sm-6">

                <div class="left_icons" id="family_disputes">
                    <div class="dxsingle_teaser_left">
                        <i class="rt-icon-users3"></i>
                    </div>
                    <div class="dxsingle_teaser_right">
                        <h3>Família</h3>
                        <p>
                            Os conflitos familiares podem resultar em grande desgaste e, não raras as vezes,
                            chegar às portas do Judiciário. Nesses momentos é muito importante contar com um advogado
                            que sensível à situação, minimize os conflitos, defendendo os seus interesses.
                        <p><a href="#footer">Solicite contato para maiores informações</a></p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="block col-sm-6">

                <div class="left_icons style2" id="copyright_disputes">
                    <div class="dxsingle_teaser_left">
                        <i class="rt-icon-bookmark"></i>
                    </div>
                    <div class="dxsingle_teaser_right">
                        <h3>Empresas</h3>
                        <p>
                            Assessoria em matéria fiscal, trabalhista, civil e administrativa podem prevenir grandes
                            embates judiciais, podendo ser evitado com orientações específicas para o planejamento, 
                            controle e eventualmente defesas administrativas ou judiciais para a sua empresa.
                            <p><a href="#footer">Solicite contato para maiores informações</a></p>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>