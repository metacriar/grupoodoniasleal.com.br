<section id="progress" class="dark_section parallax">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="block-header">Sobre o Grupo Odonias Leal</h2>
                        <p>
                        Algumas informações que nos recomendam e precedem o nome Grupo Odonias Leal
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="block col-sm-3">
                        <div class="arctcle">
                            <div class="single_teaser icons style2">
                                <div class="icons_introimg image-icon">
                                    <i class="rt-icon-ok"></i>
                                </div>
                                <h2> Mais de 800</h2>
                                <h3>Processos ganhos nos últimos anos</h3>

                            </div>
                        </div>
                    </div>

                    <div class="block col-sm-3">
                        <div class="arctcle">
                            <div class="single_teaser icons style2">
                                <div class="icons_introimg image-icon">
                                    <i class="rt-icon-chat"></i>
                                </div>
                                <h2>1000+</h2>
                                <h3>Consultas Anuais</h3>

                            </div>
                        </div>
                    </div>
                    <div class="block col-sm-3">
                        <div class="arctcle">
                            <div class="single_teaser icons style2">
                                <div class="icons_introimg image-icon">
                                    <i class="rt-icon-wallet"></i>
                                </div>
                                <h2>400+</h2>
                                <h3>Clientes Cadastrados</h3>

                            </div>
                        </div>
                    </div>
                    <div class="block col-sm-3">
                        <div class="arctcle">
                            <div class="single_teaser icons style2">
                                <div class="icons_introimg image-icon">
                                    <i class="rt-icon-pen"></i>
                                </div>
                                <h2>1200+</h2>
                                <h3>Processos em Trâmite</h3>

                            </div>
                        </div>
                    </div>
                </div></div>
        </section>
