<section id="testimonials" class="dark_section parallax">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 block">
                        <h2 class="text-center block-header">Notícias</h2>
                        <div class="carousel dxcarousel slide widget_testimonials block" id="carousel-testimonials">
                            <a data-slide="prev" href="#carousel-testimonials" class="carousel-control left"></a>
                            <a data-slide="next" href="#carousel-testimonials" class="carousel-control right"></a>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <p class="carousel-introtext">
                                        Acompanhe aqui as novidades a respeito das atividades do Grupo Odonias Leal
                                    </p>
                                    <p class="carousel-readmore">
                                        <span><img src="<?php base_url()?>assets/example/author.jpg" alt=""></span>
                                        John Doe
                                    </p>
                                </div>
                                <div class="item">
                                    <p class="carousel-introtext">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, eum adipisci asperiores animi eos sint modi amet omnis fugiat cum.
                                    </p>
                                    <p class="carousel-readmore">
                                        <span><img src="<?php base_url()?>assets/example/author.jpg" alt=""></span>
                                        Catrin Jhonson
                                    </p>
                                </div>
                                <div class="item">
                                    <p class="carousel-introtext">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, eum adipisci asperiores animi eos sint modi amet omnis fugiat cum.
                                    </p>
                                    <p class="carousel-readmore">
                                        <span><img src="<?php base_url()?>assets/example/author.jpg" alt=""></span>
                                        Nataly Smith
                                    </p>
                                </div>
                                <div class="item">
                                    <p class="carousel-introtext">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, eum adipisci asperiores animi eos sint modi amet omnis fugiat cum.
                                    </p>
                                    <p class="carousel-readmore">
                                        <span><img src="<?php base_url()?>assets/example/author.jpg" alt=""></span>
                                        Antony Gore
                                    </p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div></div>
        </section>