<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Onepage extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function _init() {
        
        $this->load->view('onepage_view');
    }

    function index() {
        $this->_init();
    }

}
